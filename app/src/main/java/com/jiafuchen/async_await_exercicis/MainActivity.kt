package com.jiafuchen.async_await_exercicis

import android.content.res.ColorStateList
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TableRow.LayoutParams
import android.widget.TextView
import androidx.core.view.marginStart
import com.jiafuchen.async_await_exercicis.databinding.ActivityMainBinding
import kotlinx.coroutines.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.button.setOnClickListener(){
            val matrix = getRandomMatrix()
            binding.table.removeAllViews()
            binding.result.text = ""
            createTable(matrix)

            val jobList = mutableListOf<Deferred<Int>>()

            matrix.forEach {
                CoroutineScope(Dispatchers.Default).launch{
                    jobList.add(async { sumList(it) })
                }
            }

            CoroutineScope(Dispatchers.Main).launch {
                binding.result.text = jobList.awaitAll().sum().toString()
            }

        }
    }

    private fun createTable(matrix : List<List<Int>>){
        val rowLayout = TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)

        matrix.forEach {itemRow ->
            val row = TableRow(this).apply {
                layoutParams = rowLayout
                gravity = Gravity.CENTER
            }

            itemRow.forEach {item ->

                val textLayout = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT).apply {
                    marginStart = 10
                    marginEnd = 10
                }

                val text = TextView(this).apply {
                    this.layoutParams = textLayout
                    this.text = item.toString()
                    this.setTextColor(Color.BLACK)
                }

                row.addView(text)
            }

            binding.table.addView(row)
        }


    }


    private fun getRandomMatrix() : List<List<Int>>{
        val x = (1..10).random()
        val y = (2..10).random()
        return List(y){ List(x){ (0..100).random() } }

    }

    suspend fun sumList(list : List<Int>) : Int {
        var result = 0
        delay(100)
        list.forEach { result += it }
        return result
    }
}
